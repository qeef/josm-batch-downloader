# JOSM Batch Downloader

Downloads data around every selected feature or inside of it (Polygons) or around each feature in active layer if none are selected.

The tool works in a loop and creates a rectangle around each (selected) feature and runs a download task with the created 
rectangle as a bounding box. The size of the rectangle around the feature can be specified.
There are 2 main purposes:

1. Download data around imported points for further validation. (This was the original purpose and much larger project. 
You can check it out on https://gitlab.com/Jamalek/survey-validator)

2. Multiple task validation with HOTOSM Tasking Manager: 
	a)	Go to a project, select validation tab without any tile selected (or click outside tiled area to unselect any 
	tile)
		Here you can see all mappers of the project.
	b)	Click start on any mapper to start validating all his tiles.
	c)	Click on start JOSM as normally. Here instead of sending the data, TM will only send the bounding boxes. 
        (Only works for certain number of tiles, tested 300 - worked, 1400 - did not work)
	d)	Activate the layer containing tiles
	e)	Run Batch-downloader from menu/Data/Batch download
	
Each feature usually takes up something between half a second to several seconds depending on the size of the downloaded 
area. Keep that in mind when downloading data around more than 100 features.

Example video: https://www.youtube.com/watch?v=lm3jK4VuUOo (old version)

You can contact me on 2janmalek[at]gmail.com
