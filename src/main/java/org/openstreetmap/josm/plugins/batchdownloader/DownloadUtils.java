package org.openstreetmap.josm.plugins.batchdownloader;

import org.openstreetmap.josm.actions.downloadtasks.DownloadOsmTask;
import org.openstreetmap.josm.actions.downloadtasks.DownloadParams;
import org.openstreetmap.josm.data.Bounds;
import org.openstreetmap.josm.data.osm.BBox;
import org.openstreetmap.josm.data.osm.DataSet;
import org.openstreetmap.josm.data.osm.Node;
import org.openstreetmap.josm.data.osm.OsmPrimitive;
import org.openstreetmap.josm.gui.ExtendedDialog;
import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.gui.layer.OsmDataLayer;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Future;

import static org.openstreetmap.josm.tools.I18n.tr;

public class DownloadUtils {
  public static void downloadAroundFeatures(List<OsmPrimitive> features, double dist, boolean newLayer) {
    if (newLayer) {
      createAndActivateLayer();
    }
    for (OsmPrimitive feature : features) {
      download(feature.getBBox(), dist);
    }
  }

  private static void createAndActivateLayer() {
    OsmDataLayer dataLayer = new OsmDataLayer(new DataSet(), "Batch downloaded", null);
    MainApplication.getLayerManager().addLayer(dataLayer);
    MainApplication.getLayerManager().setActiveLayer(dataLayer);
  }

  public static void download(BBox bBox, double dist) {
    Bounds downloadBounds = BoundsUtils.fromBbox(bBox);
    Bounds downloadBoundsExtended = BoundsUtils.add(downloadBounds, dist);
    DownloadOsmTask task = new DownloadOsmTask();
    Future<?> future = task.download(new DownloadParams(), downloadBoundsExtended, null);
    MainApplication.worker.submit(() -> {
      try {
        future.get();
      } catch (Exception ignored) {}
    });
  }
}
